module.exports = {
  'env': {
    'commonjs': true,
    'es6': true,
    'node': true,
  },
  'extends': [
    'google',
  ],
  'globals': {
    'Atomics': 'readonly',
    'SharedArrayBuffer': 'readonly',
  },
  'parserOptions': {
    'ecmaVersion': 2018,
  },
  'rules': {
    'linebreak-style': 0,
    'max-len': [1, { 'code': 120 }],
    'padded-blocks': 0,
    'object-curly-spacing': [2, 'always'],
    'require-jsdoc': 0,
    'indent': [2, 2, { "SwitchCase": 1 }],
    'no-undef': 2,
  },
};
