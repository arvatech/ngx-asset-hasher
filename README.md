# Hashing filenames for Angular assets

Hashes all filenames in `/src/assets` and also looks for possible replacements and orphans.

---

Made by [ARVA Tech GmbH](https://arva.tech)
