#!/usr/bin/env node

const path = require('path');
const crypto = require('crypto');
const fs = require('fs');
const readline = require('readline');
const util = require('util');
const klawSync = require('klaw-sync');

const hashLength = 6;

const sourceFolder = path.join(
  process.cwd(),
  'src',
);

const getFiles = async () => {

  const folder = path.join(
    process.cwd(),
    'src',
    'assets',
  );

  const files = klawSync(folder, {
    nodir: true,
    filter: (item) => path.basename(item.path) !== '.gitkeep',
  });

  return { files, folder };
};

const createFileHash = async (files) => {

  const promises = [];

  const renameFilesMap = [];

  const regex = new RegExp(`\\.([a-z0-9]{${hashLength}})$`);

  for (const file of files) {

    promises.push(new Promise((resolve, reject) => {
      const hash = crypto.createHash('sha1');
      const input = fs.createReadStream(file.path);
      input.on('readable', () => {
        // Only one element is going to be produced by the
        // hash stream.
        const data = input.read();
        if (data) {
          hash.update(data);
        } else {

          const hashValue = hash.digest('base64').replace(/[^a-z0-9]/gi, '').substr(0, hashLength).toLowerCase();

          const extname = path.extname(file.path);
          let filename = path.basename(file.path, extname);

          // check if file already has an hash
          const match = filename.match(regex);
          if (match && match[1] === hashValue) {
            return resolve();
          } else {

            if (match) {
              filename = filename.replace(match[0], '');
            }

            const newPath = path.join(
              path.dirname(file.path),
              `${filename}.${hashValue}${extname}`
            );

            renameFilesMap.push({
              old: file.path,
              new: newPath,
            });
            resolve();
          }
        }
      });
      input.on('error', (error) => reject(error));
    }));

  }

  await Promise.all(promises);

  return renameFilesMap;
};

const renameFiles = async (renameFilesMap) => {

  const promises = [];

  for (const file of renameFilesMap) {
    promises.push(new Promise((resolve, reject) => fs.rename(
      file.old,
      file.new,
      (error) => error ? reject(error) : resolve(),
    )));
  }

  await Promise.all(promises);
};

const waitForInput = async (question) => {

  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  return await new Promise((resolve, reject) => rl.question(question, (answer) => {
    rl.close();
    resolve(answer.trim());
  }));

};

const searchCode = async (renameFilesMap) => {

  const files = klawSync(sourceFolder, {
    nodir: true,
    filter: (item) => path.basename(item.path) !== 'assets',
  });

  const codeSearchResult = [];

  for (const renameFile of renameFilesMap) {

    // faster than indexOf
    const regex = RegExp(path.basename(renameFile.old));

    const matches = [];

    for (const file of files) {

      const sourceCode = fs.readFileSync(file.path, 'utf8');

      if (regex.test(sourceCode)) {

        const lines = sourceCode.split(/\r?\n/);

        const fileMatch = {};

        for (let i = 0; i < lines.length; i++) {
          if (regex.test(lines[i])) {
            fileMatch[`L${i + 1}`] = lines[i];
          }
        }
        matches.push({
          file: file.path,
          lines: fileMatch,
        });
      }
    }

    if (matches.length > 0) {
      codeSearchResult.push({
        old: path.basename(renameFile.old),
        new: path.basename(renameFile.new),
        matches,
      });
    }
  }

  return codeSearchResult;
};

const replaceCode = async (codeSearchResult) => {

  for (const resultEntry of codeSearchResult) {

    for (const match of resultEntry.matches) {

      fs.writeFileSync(
        match.file,
        fs.readFileSync(match.file, 'utf8').replace(
          RegExp(escapeRegExp(resultEntry.old), 'g'),
          resultEntry.new,
        ),
      );

    }

  }

};

const searchDeletedAssets = async () => {

  const sourceFiles = klawSync(sourceFolder, {
    nodir: true,
    filter: (item) => path.basename(item.path) !== 'assets',
  });

  // faster than indexOf
  const regex = RegExp(`assets/.*?([^/]+\\.[a-z0-9]{${hashLength}}\\.[0-9A-Za-z]+)`);

  const matches = [];

  const { files } = await getFiles();

  const exisitngAssetBasenames = [];
  for (const file of files) {
    exisitngAssetBasenames.push(path.basename(file.path));
  }

  for (const file of sourceFiles) {

    const sourceCode = fs.readFileSync(file.path, 'utf8');

    if (regex.test(sourceCode)) {

      const lines = sourceCode.split(/\r?\n/);

      const fileMatch = {};

      for (let i = 0; i < lines.length; i++) {

        const match = lines[i].match(regex);

        if (match) {
          if (!exisitngAssetBasenames.includes(match[1])) {
            fileMatch[`L${i + 1}`] = lines[i];
          }
        }
      }
      if (Object.keys(fileMatch).length > 0) {
        matches.push({
          file: file.path,
          lines: fileMatch,
        });
      }

    }

  }

  return matches;
};

const escapeRegExp = (string) => {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
};

(async () => {

  console.log('Add hash to all asstes files');

  const { files, folder } = await getFiles();
  console.log(`Found [${files.length}] files in folder ${folder}`);

  const renameFilesMap = await createFileHash(files);
  console.log('SHA1 Hex hashes are generated and trimmed to 20 characters.');

  if (renameFilesMap.length === 0) {
    console.log('All hashes up-to-date. Nothing to do.');
  } else {

    console.log(util.inspect(renameFilesMap, false, 99, true));

    const answerRename = await waitForInput(`Do you really want to rename ${renameFilesMap.length} file(s)? [y/N]: `);

    if (answerRename.match(/y(es)?/i)) {
      await renameFiles(renameFilesMap);
      console.log('Finished renaming.');
    } else {
      console.log(' --- Renaming aborted --- ');
      return;
    }

    // eslint-disable-next-line max-len
    const answerSearchCode = await waitForInput('Should we search for the renamed file names in the source code? [Y/n]: ');

    if (answerSearchCode.match(/n(o)?/i)) {
      console.log(' --- Source code searching aborted --- ');
      return;
    }

    const searchCodeResult = await searchCode(renameFilesMap);
    console.log(util.inspect(searchCodeResult, false, 99, true));

    if (searchCodeResult.length === 0) {
      console.log('Source code is up-to-date. Nothing to do.');
    } else {

      // eslint-disable-next-line max-len
      const answerReplace = await waitForInput(`Do you really want change the code in ${searchCodeResult.length} file(s)? [y/N]: `);

      if (answerReplace.match(/y(es)?/i)) {
        await replaceCode(searchCodeResult);
        console.log('Finished source code replacing.');
      } else {
        console.log(' --- Source code replacing aborted --- ');
      }

    }

  }

  // eslint-disable-next-line max-len
  const answerOrphanSearch = await waitForInput('Should we search for asset orphans (deleted assets) in the code? [Y/n]: ');

  if (answerOrphanSearch.match(/n(o)?/i)) {
    console.log(' --- Deleted search aborted --- ');
    return;
  } else {
    const searchDeletedResult = await searchDeletedAssets();
    if (searchDeletedResult.length > 0) {
      console.log(util.inspect(searchDeletedResult, false, 99, true));
      console.log(`Found ${searchDeletedResult.length} asset orphans.`);
    } else {
      console.log('No asset orphans found.');
    }
  }

})();
